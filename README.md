# Tweet Generator
This is entirely for educational purposes. Generated text will be purely for own use.

## Features

- A user-friendly menu in command line for managing and training a neural network for multiple twitter sources
- Train a model specifically for any twitter user
- Generate tweets based on that model
- Output generated tweets to a text file
- Customized options for generating tweets:
  - Max characters
  - How many lines
  - Creativity factor (0.2 - 1.0)
  - Start a sentence with some words and the model will predict preceding words

**Things to add or change**

- Allow tweets from an account be appended to previous data. Currenlty older tweets are not saved
- Make a check to remove duplicate tweets
- View ID (4)
- Remove / edit ID
- Look into different ways of making a menu in python if this gets more complicated
- Currently too many tweets are filtered out. >1MB of training data would be optimal
- If the loss factor gets below 0.5, the generated text will be too similar to the training data even with a creativity factor of 1.0.
- Similarity index between generated text and original

### Menu
```
0:  Exit
1:  Generate
2:  Add ID / Update ID
3:  List ID's
4:  View ID
5:  Train

```

(the order of these options might change later on)

## Usage
Run: `python3.7 main.py`

1. Add a source. All ID's in the program are case sensitive
2. All available tweets will be shown. The number on the left is a counter. Tweets are extracted into a text file in `data/`
3. Change config.yml to your liking. Use higher `epoch` if you wish to train for longer
4. Train
5. Generate


## Installation 
**Pre-requirements**

1. python version 3.6-3.7
```
python --version
```
If the command above returns a value that is not `3.7.x`, install an older python version. Python is picky with what python or pip you use. For example, if you install some package on pip3.8, it might not be available on pip3.6 or 3.7. Therefore, use:
- pip -> pip3.7
- python -> python3.7
-----
2. You need to install packages via pip
open terminal and type
- Mac: `brew install pip` you need [homebrew](https://brew.sh/)
- Windows: [click here](https://www.liquidweb.com/kb/install-pip-windows/)

3. Makefile: run this in terminal
-----
Note that I haven't yet tested installing this on a fresh system. There might be errors that I'm unaware of.

```bash
git clone https://gitlab.com/keklas/tweet-generator.git
cd tweet-generator
make -si
```

## Training on GPU
[tensorflow gpu support](https://www.tensorflow.org/install/gpu)

Getting this to work was a major challenge of its own. Here's some tips from two weeks of why this didn't work. Hopefully it leads you to the right direction:

### Initial words

Note that my might be different than yours and thus the requirements for your system might be different. I use Nvidia GTX 1080 with ArchLinux.

### Debugging tensorflow messages

You can supress most of these errors by:

```python
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
```

>`Could not load dynamic library 'libnvinfer.so.6'`, `Could not load dynamic library 'libnvinfer_plugin.so.6' OR something similar to libnvinfer`

These are part of TensorRT  that are optional. **IF** you are getting errors for libraries other than these two, then you are missing some vital components.

---


>`Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA`

Not an issue **IF** there is something about your GPU after this, for example:

>`Found device 0 with properties:`
>`pciBusID: 0000:01:00.0 name: GeForce GTX 1080 computeCapability: 6.1`
>`coreClock: 1.8225GHz coreCount: 20 deviceMemorySize: 7.93GiB deviceMemoryBandwidth: 298.32GiB/s`

---
>`successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero`

I forgot what this ment but its nothing important. It may have been something about running multiple GPUs.

---
>`WARNING:tensorflow:sample_weight modes were coerced from ... to '...'`

No idea what this means, but it doesnt prevent you from training

---

### Other tips

Don't compile from source code. You'll just end up ripping your hair out

---

I tried everything on Ubunutu 18.04LTS, didnt work. I use arch and not Ubuntu, so it might have been my lack of knowledge as well. I remember getting errors like:

>`Could not load dynamic library libcudart.so.10.1`

>`Could not load dynamic library libcublas.so.10`

>`...`

>`Could not load dynamic library libcusparse.so.10`

even though I had cuda and cudnn installed?

---

As of 4.3.2020, `tensorflow 2.1.0` is not in pip3.8, use pip3.7

---

Install __all__ python packages via pip. Dont bother with `python-tensorflow` in `pacman` or `brew`

---

