#!/usr/bin/python3.7
import sys, os, re, tweepy, yaml
import traceback2 as traceback

# Supress pointless log messages from tensorflow
# Set to a lower value if you need to debug issues in tensorflow
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from textgenrnn import textgenrnn

# Main aboslute root directory
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

#--------------------
# Functions below
#--------------------

# Function for connecting to the twitter API
def loadAPI():
    os.chdir(ROOT_DIR)

    # Read configuration file
    with open("../keys.yml", "r") as f:
        keys = yaml.load(f, Loader=yaml.BaseLoader)
    try:
        auth = tweepy.OAuthHandler(keys['consumer_key'], keys['consumer_secret'])
        auth.set_access_token(keys['access_key'], keys['access_secret'])

        api = tweepy.API(auth)
        api.verify_credentials()
        print("API auth OK")
        return api

    except:
        print("Error in authentication. Have you added keys to keys.yml file?")
        sys.exit(EX_CONFIG)

# Loads configuration for training
def loadConf():
    os.chdir(ROOT_DIR)
    with open("../config.yml", "r") as f:
        cfg = yaml.load(f, Loader=yaml.BaseLoader)
        return cfg

# Used for checking if data directories exist
def containsDir(parent_dir, child_dir):
    try:
        os.chdir(ROOT_DIR)
        dirs_list = []
        init_dir = os.path.abspath(os.curdir)
        os.chdir(parent_dir)
        path = os.path.abspath(os.curdir)
        for root,dirs,files in os.walk(path):
            for d in dirs:
                dirs_list.append(d)
        if child_dir in dirs_list:
            return True
        else:
            return False
    except:
        print(traceback.print_exc())
        return False

# Checks if some files exist
def containsFile(usrID, file_type):
    path = os.path.abspath(os.path.join(ROOT_DIR,"..","data",usrID))
    # List of found files
    ff = []
    try:
        os.chdir(path)
        for root,dirs,files in os.walk(path):
            for f in files:
                ff.append(f)

        if (usrID + file_type in ff) | (usrID+"_weights"+file_type in ff):
            return True
        else:
            print("Some files not found")
            return False
    except:
        return False

# Checks if account on twitter exists
def accountExists(usrID):
    print("Checking for account in Twitter")
    api = loadAPI()
    try:
        api.get_user(usrID)
        print(usrID + " found!")
        return True
    except:
        print(usrID + " account not found!")
        return False

# Filters data from TwitterAPI
def process_tweet_text(text):
    text = str(text)
    text = re.sub(r'http\S+', '', text)   # Remove URLs
    text = re.sub(r'@[a-zA-Z0-9_]+', '', text)  # Remove @ mentions
    text = text.strip(" ")   # Remove whitespace resulting from above
    text = re.sub(r' +', ' ', text)   # Remove redundant spaces

    # Handle common HTML entities
    text = re.sub(r'&lt;', '<', text)
    text = re.sub(r'&gt;', '>', text)
    text = re.sub(r'&amp;', '&', text)
    return text

# Extracts text from twitter
def extractText(usrID):
    tweets = []
    try:
        api = loadAPI()
    except Exception:
        print("Error in twitter authentication: \n",traceback.print_exc(), sep="\n")
    try:
        pages = tweepy.Cursor(api.user_timeline, screen_name=usrID, count=400, tweet_mode="extended", include_rts=False, exclude_replies=True).pages(50)
        for page in pages:
            for tweet in page:
                text = process_tweet_text(tweet.full_text)
                if text != '':
                    tweets.append(text)
                    print(str(len(tweets)), text)
        return tweets
    except Exception:
        print("Twitter error response: status code = 404. Account not found")
        print(traceback.print_exc())

# Create new directory for a new source
def addDir(usrID):
    dir_path = os.path.abspath(os.path.join(ROOT_DIR,"..","data",usrID))
    print("Creating a directory for {} in {}".format(usrID, dir_path))
    os.mkdir(dir_path)

# Write tweets from API to text file. Used in training
def saveTXT(list, dest):
    print("Saving txt file to: " + dest)
    try:
        with open(dest, "w") as f:
            for tweet in list:
                f.write("%s\n" % tweet)
    except:
        print("Writing to " + dest + " failed")

# VICTOR / TOMI
def getFileSize(usrID):
    e = 0

    # Return local .txt file size for any twitter user

# VICTOR / TOMI
def getFollowers(usrID):
    e = 0

    # Return twitter follower count in string format


# Here is the main function of the program
if __name__== "__main__":
    while True:

        # Instructions
        print("")
        print("0:  Exit")
        print("1:  Generate")
        print("2:  Add ID / Update ID")
        print("3:  List ID's")
        print("4:  View ID")
        print("5:  Train")

        # Main menu
        try:
            option = int(input("Selection: "))

            # END =====================================
            if(option == 0):
                break

            # GENERATE =====================================
            elif(option == 1):
                print("")
                usrID = input("Give twitter ID: ")
                if(containsDir("../data", usrID) & containsFile(usrID, ".txt")):
                    try:
                        if(containsFile(usrID, ".txt") & (containsFile(usrID, "_weights.hdf5"))):
                            try:
                                count = int(input("How many lines: "))
                            except:
                                print("Not a number, defaulting to 1")
                                count = 1
                            try:
                                temp = float(input("How much creativity should be used? Between 0.1 and 1.0: "))
                                if(temp > 1.0) | (temp < 0.1):
                                    print("Incorrect range, defaulting to 0.2")
                                    temp = 0.2
                            except:
                                print("Not a decimal, defaulting to 0.2")
                                temp = 0.2
                            try:
                                max_length = int(input("Max characters: "))
                            except:
                                print("Not a number, defaulting to 300")
                                max_length = 300

                            print("How should the sentence start? The neural network will complete it")
                            print("For example: ")
                            print("\t Why does the...")
                            print("\t Trump...")
                            print("\t 5...\n")
                            words = input("Prefix: (skip with empty)")

                            output_path = os.path.abspath(os.path.join(ROOT_DIR,"..","output",usrID+".txt"))
                            weig_path = os.path.abspath(os.path.join(ROOT_DIR,"..","data",usrID+"/"+usrID+"_weights.hdf5"))
                            conf_path = os.path.abspath(os.path.join(ROOT_DIR,"..","data",usrID+"/"+usrID+"_config.json"))
                            voca_path = os.path.abspath(os.path.join(ROOT_DIR,"..","data",usrID+"/"+usrID+"_vocab.json"))
                            textgen = textgenrnn(name=usrID, weights_path=weig_path, vocab_path=voca_path, config_path=conf_path)

                            if(words):
                                print("Count: {}, Prefix: {}, Creativity: {}, Max length: {} ".format(count,words,temp,max_length))
                                textgen.generate_to_file(output_path, n=count, prefix=words, temperature=temp, max_gen_length=max_length, progress=True)
                            else: 
                                print("Count: {}, Creativity: {}, Max length: {} ".format(count,temp,max_length))
                                textgen.generate_to_file(output_path, n=count, temperature=temp, max_gen_length=max_length, progress=True)
                        else:
                            print("Training data is missing. Please train the network (5) with the user " + usrID)
                    except:
                        print("Error in generating text")
                        print(traceback.print_exc())
                else:
                    print("Account " + usrID + " has not been downloaded yet. Please add ID(2)")

            # ADD ID ===================================== update not yet working
            elif(option == 2):
                print("")
                usrID = input("Give twitter ID: ")
                try:
                    if(containsDir("../data", usrID)):

                        # Sample data exists
                        if(containsFile(usrID, ".txt")):
                            print("Files for " + usrID + " already exist")
                            print("Replacing data")

                            data = extractText(usrID)
                            write_path = os.path.abspath(os.path.join(ROOT_DIR,"..","data",usrID+"/"+usrID+".txt"))
                            saveTXT(data, write_path)

                    # Check if account exists in twitter
                    elif(accountExists(usrID)):
                        addDir(usrID)
                        data = extractText(usrID)
                        write_path = os.path.abspath(os.path.join(ROOT_DIR,"..","data",usrID+"/"+usrID+".txt"))
                        saveTXT(data, write_path)

                # If anything above fails
                except Exception:
                    print("No local data for " + usrID + " found")
                    print(traceback.print_exc())

            # LIST SAVED ID'S =====================================
            elif(option == 3):
                print("")
                accounts = []
                os.chdir("../data")
                path_to_data = os.path.abspath(os.curdir)
                for root,dirs,files in os.walk(path_to_data):
                    for dir in dirs:
                        accounts.append(dir)
                print(accounts)

            # VIEW ID =====================================
            elif(option == 4):
                print("")
                api = loadAPI()

            # TRAIN =====================================
            elif(option == 5):
                print("")
                print("Training is most efficient on a GPU")
                print("")
                conf = loadConf()
                print(conf['model_config'])
                usrID = input("Give twitter ID: ")
                print("")

                main_conf_path = os.path.abspath(os.path.join(ROOT_DIR,"../config.yml"))
                text_path = os.path.abspath(os.path.join(ROOT_DIR,"..","data",usrID+"/"+usrID+".txt"))
                weig_path = os.path.abspath(os.path.join(ROOT_DIR,"..","data",usrID+"/"+usrID+"_weights.hdf5"))
                try:
                    # If User ID is found
                    if(containsDir("../data", usrID)):

                        # If text data and previous training data is found, continue training
                        if(containsFile(usrID, ".txt") & containsFile(usrID, ".hdf5")):
                            print("Text and training data found!")
                            print("Training duration: {} epochs. ")
                            print("Samples generated every: {} epochs. ")

                            # Absolute paths to training data
                            conf_path = os.path.abspath(os.path.join(ROOT_DIR,"..","data",usrID+"/"+usrID+"_config.json"))
                            voca_path = os.path.abspath(os.path.join(ROOT_DIR,"..","data",usrID+"/"+usrID+"_vocab.json"))

                            textgen = textgenrnn(name=usrID, weights_path=weig_path, vocab_path=voca_path, config_path=conf_path)
                            textgen.train_from_file(text_path,
                                                    num_epochs=int(conf['num_epochs']),
                                                    gen_epochs=int(conf['gen_epochs']),
                                                    new_model=False)
                            textgen.save(weights_path=weig_path)

                        # If only text data us is found, make a new traing model
                        elif(containsFile(usrID, ".txt")):
                            print("Only text data found. Creating new training weights and vocabulary data for " + usrID)
                            print("Training duration: {} epochs. ")
                            print("Samples generated every: {} epochs. ")

                            nconf = conf['model_config']
                            textgen = textgenrnn(name=usrID)
                            textgen.train_from_file(text_path,
                                                    num_epochs=int(conf['num_epochs']),
                                                    gen_epochs=int(conf['gen_epochs']),
                                                    new_model=True,
                                                    rnn_layers=int(nconf['rnn_layers']),
                                                    rnn_size=int(nconf['rnn_size']),
                                                    max_length=int(nconf['max_length']),
                                                    dim_embeddings=int(nconf['dim_embeddings']))

                            textgen.save(weights_path=weig_path)
                except:
                    print("Error in training")
                    print(traceback.print_exc())

        # Do this if errors in menu
        except Exception:
            print("Error in menu", " ", traceback.print_exc(), sep="\n")
